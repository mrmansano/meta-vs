DESCRIPTION = "Ambiente generico de produtos Visio Satis" 
SECTION = "visiosatis" 
LICENSE = "CLOSED" 
DEPENDS = " \
            vs-firmwareupdate-qemu \ 
            vs-framecatcher-qemu \ 
            vs-dispatcher-qemu \
            vs-exchanger-qemu \
            vs-motorlens-qemu \
            iptables \
            "


SRC_URI = "git://git@${GIT_SERVER_IP}/git/vs_common/vs_ecosys.git;branch=sto0303_sqlite3_qemu_confict;protocol=ssh;tag=90d1e566a0510474a3e042e30547c342fd6ee7d2"

S = "${WORKDIR}/git/"

## Specify the files for each package
FILES_${PN}-dbg += "/usr/bin/tests/.debug/*"

FILES_${PN}-rootfs += "/usr/vs/*"
FILES_${PN}-rootfs += "/home/root/*"

FILES_${PN}-tests += "/usr/bin/tests/*"

FILES_${PN}-dev += "${libdir}/*.so*"

FILES_${PN}-staticdev += "/usr/lib/static/*.a"

OECMAKE_RPATH = "/usr/lib,/lib"


## Configure the output packages
PACKAGES = " \
    ${PN}-dbg \
    ${PN}-rootfs \
    ${PN} \
    ${PN}-doc \
    ${PN}-dev \
    ${PN}-staticdev \
    ${PN}-tests \
    "

## Extra make parameters
EXTRA_OECMAKE = " \
    -DVIGILANT_MODEL=5 \
    -DVIGILANT_FAMILY=5 \
    -DBUILD_PCRE=OFF \
    -DBUILD_SQLITE3=OFF \
"

inherit update-rc.d cmake

INITSCRIPT_PACKAGES = "${PN}"
INITSCRIPT_NAME_${PN} = "vs-ecosys"
INITSCRIPT_PARAMS_${PN} = "defaults 80"

do_install_prepend() {
    ## Adiciona o script de inicializacao
	install -d ${D}${sysconfdir}/init.d
	install -m 0755 ${S}/root_filesys/usr/bin/vs_ecosys.init ${D}${sysconfdir}/init.d/vs-ecosys
}

do_install_append() {
    ## Removo as bibliotecas .la
    find ${D} -name "*.la" -exec rm {} \;

##    rm -f ${D}/etc/init.d/vs-ecosys
    mkdir -p ${D}/usr/vs/db
}
