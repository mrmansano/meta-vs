DESCRIPTION = "Biblioteca frameCatcher de acesso a hardware para QEMU" 
SECTION = "visiosatis" 
LICENSE = "CLOSED" 
DEPENDS = "opencv"

## Specify the files for each package
FILES_${PN} += " \
    /usr/share/* \
    "

## Configure the output packages
PACKAGES = " \
    ${PN}-dbg \
    ${PN} \
    ${PN}-doc \
    ${PN}-dev \
    ${PN}-staticdev \
    "

SRC_URI = "git://git@${GIT_SERVER_IP}/git/vs_common/vs_ecosys_app.git;protocol=ssh;branch=v3.0;tag=169b5f8bbf7e1172affa0163af6a27597f8d82c2"

FILES_${PN} += "${libdir}/lib*"
PACKAGES = "${PN}-dbg ${PN} ${PN}-doc ${PN}-dev"

S = "${WORKDIR}/git/framecatcherHW"

inherit cmake
