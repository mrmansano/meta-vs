SMMARY = "Simple opencv hello world application"
SECTION = "examples"
DEPENDS = "opencv"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = " \
    file://vs-ocv-hello.cpp \
"

S = "${WORKDIR}"

do_compile() {
    ${CXX} vs-ocv-hello.cpp `pkg-config --cflags --libs opencv` -o vs-ocv-hello
}

do_install() {
    install -d ${D}${bindir}
    install -m 0755 vs-ocv-hello ${D}${bindir}
}
