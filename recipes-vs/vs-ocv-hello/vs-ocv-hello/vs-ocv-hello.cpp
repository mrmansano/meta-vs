#include <stdio.h>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace cv;

int main(int argc, char *argv[])
{
    if (argc < 3) {
        printf("Converte uma imagem colorida para uma imagem preto e branco\n");
        printf("Uso: %s img_in img_out\n", argv[0] );
        return 0;
    }

    char *img_in  = argv[1];
    char *img_out = argv[2];


    printf("Abrindo imagem: %s\n", img_in);
    cv::Mat img = cv::imread(img_in, CV_LOAD_IMAGE_GRAYSCALE); 

    cv::Mat bw = img > 128;

    printf("Salvando imagem: %s\n", img_out);
    cv::imwrite(img_out, bw);

    return 0;
}
