DESCRIPTION = "Driver de exchanger par o QEMU" 
SECTION = "visiosatis" 
LICENSE = "CLOSED" 

SRC_URI = "git://git@${GIT_SERVER_IP}/git/vs_common/vs_drivers.git;branch=v3.0;protocol=ssh;tag=b69058881647bf0d408e8241edc9c0faf212c857"

S = "${WORKDIR}/git/"

## Add the header to the main package
FILES_${PN} += "/usr/include/*"

PACKAGES = " \
    ${PN}-dbg \
    ${PN} \
    ${PN}-doc \
    ${PN}-dev \
    ${PN}-staticdev \
    "


do_install () {
    mkdir -p ${D}${includedir}/
    cp ${S}/exchanger/exchanger.h ${D}${includedir}/
}
