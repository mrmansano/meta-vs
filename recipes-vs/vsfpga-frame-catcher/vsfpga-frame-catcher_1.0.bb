DESCRIPTION = "Gerenciador de frames do Vigilant FPGA" 
SECTION = "examples" 
LICENSE = "CLOSED" 
PR = "r0" 

SRCREV = "1e8b5d654eac275c5ca5b6b6d1f4b69dde0410e1"
SRC_URI = "git://git@${GIT_SERVER_IP}/git/vscam_fpga/vscam_fpga_app.git;protocol=ssh"

S = "${WORKDIR}/git/vigilant_frame_catcher"

inherit cmake
