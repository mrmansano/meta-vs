SUMMARY = "Visio Satis VigilantFPGA image."

IMAGE_INSTALL = "packagegroup-core-boot ${ROOTFS_PKGMANAGE_BOOTSTRAP} ${CORE_IMAGE_EXTRA_INSTALL}"

IMAGE_LINGUAS = " "

#LICENSE = "GPLv2+"
LICENSE = "MIT"

inherit core-image

IMAGE_ROOTFS_SIZE ?= "8192"

## Configure the extra features and programs to install
EXTRA_IMAGE_FEATURES += "ssh-server-openssh"

CORE_IMAGE_EXTRA_INSTALL += "libopencv-core"
CORE_IMAGE_EXTRA_INSTALL += "screen"
CORE_IMAGE_EXTRA_INSTALL += "python-core"
CORE_IMAGE_EXTRA_INSTALL += "lighttpd  \
                             lighttpd-module-access \
                             lighttpd-module-alias \
                             lighttpd-module-accesslog \
                             lighttpd-module-cgi \
                             lighttpd-module-indexfile \
                             lighttpd-module-dirlisting \
                             lighttpd-module-staticfile \
"

CORE_IMAGE_EXTRA_INSTALL += "vsfpga-frame-catcher"

## Set license to commercial to enable libav to be compiled
## This should be set in local.conf
## LICENSE_FLAGS_WHITELIST += "commercial"
