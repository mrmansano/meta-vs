SUMMARY = "Visio Satis Vigilant FPGA image."

IMAGE_INSTALL = "packagegroup-core-boot ${ROOTFS_PKGMANAGE_BOOTSTRAP} ${CORE_IMAGE_EXTRA_INSTALL}"

IMAGE_LINGUAS = " "

#LICENSE = "GPLv2+"
LICENSE = "MIT"

inherit core-image

IMAGE_ROOTFS_SIZE ?= "8192"

## Configure the extra features and programs to install
EXTRA_IMAGE_FEATURES += "ssh-server-openssh"

CORE_IMAGE_EXTRA_INSTALL += "libopencv-core"
CORE_IMAGE_EXTRA_INSTALL += "screen"
CORE_IMAGE_EXTRA_INSTALL += "python-core"
CORE_IMAGE_EXTRA_INSTALL += "iptables"
CORE_IMAGE_EXTRA_INSTALL += "rsync"
CORE_IMAGE_EXTRA_INSTALL += "openssl"
CORE_IMAGE_EXTRA_INSTALL += "valgrind"
CORE_IMAGE_EXTRA_INSTALL += "vs-ecosys vs-ecosys-dev vs-ecosys-rootfs"

## Set license to commercial to enable libav to be compiled
## This should be set in local.conf
## LICENSE_FLAGS_WHITELIST += "commercial"
