Visio Satis Yocto Meta repository
=================================

![VisioSatisSymbol](simbolo.png)

** Warning: This is a detached HEAD from the main project Visio Satis Meta to
publish the code in an open repository from bitbucket. **

This is the meta repository to be used to build the Visio Satis systems with the
Yocto build systems.

# Recipes

This repository compreends the following recipes:

## Core:

### vs-ecosys-image
  Recipe to bitbake an entire image to be used at the Visio Satis Vigilant IPU
  (without the FPGA) with the main components.


### vs-fpga-image
  Recipe to bitbake an entire image to be used at the Visio Satis Vigilant IPU
  with the Altera Cyclone IV FPGA image and with the main software components.

## Visio Satis components

### vs-dispatcher-QEMU
  Recipe to build the emulated hardware interface module.

### vs-ecosys
  Recipe to build the main system controller.

## vs-exchanger
  Recipe to build the system network data interface.

## vs-firmwareupdate-qemu
  Recipe to build the firmware update system.

## vs-framecatcher-qemu
  Recipe to build the emulated module that communicates with the camera and grab frames.

## vs-motorlens
  Recipe to build the module that handles the camera configuration.

## vsfpga-frame-catcher
  Recipe to build the emulated module that communicates with the camera, grab frames and sends to the FPGA.
